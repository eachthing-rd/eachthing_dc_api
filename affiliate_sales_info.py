#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import pymysql

hostip = 'datacontexts.cqb1mv8v62cx.eu-north-1.rds.amazonaws.com'
user = 'randd'
password = 'szrlynudl6l9gpywdktn'
database = 'meta'
database_two = 'affiliate_sales'


# Main function
def lambda_handler(event, context):
    con = pymysql.connect(host=f'{hostip}',
                          user=f'{user}',
                          password=f'{password}',
                          database=f'{database_two}')
    # parse out incomming me
    gtin_request = event['product_aliases']
    gtins = str()
    for g in gtin_request:
        gtins += f"'{g}', "
    gtins = gtins[:-1]
    gtins = gtins[:-1]
    cursor = con.cursor()
    sql_query = f"SELECT d1.gtin, d2.retailer_name, d1.product_data " \
                f"FROM scrapes d1 " \
                f"JOIN feeds d2 " \
                f"ON d1.feed_id = d2.id " \
                f"WHERE gtin in ({gtins}) "
    cursor.execute(sql_query)
    request_tuple = cursor.fetchall()
    con.close()
    # create responseObject
    responseObject = {}
    # create requested responseobject by packing relevant data from bulk query
    not_in_stock = ('out of stock', '-1', 'Nej', 'nej', '0')
    responseObject = {}
    for c, x in enumerate(request_tuple, 1):
        k, r, v = x
        v = v.replace("\n", "")
        product_data = json.loads(v)
        responseObject[c] = {}
        responseObject[c]['retailer'] = str(r)
        if product_data['meta']['stockAmount'] in not_in_stock:
            responseObject[c]['stock_amount'] = -1
        elif not product_data['meta']['stockAmount']:
            responseObject[c]['stock_amount'] = 0
        else:
            responseObject[c]['stock_amount'] = 1
        responseObject[c]['retailer_price'] = {}
        no_price = ('0.00', '0', '0.0')
        if product_data['product']['price']['current'] in no_price:
            pass
        else:
            responseObject[c]['retailer_price']['current'] = product_data['product']['price']['current']
        if product_data['product']['price']['original'] in no_price:
            pass
        else:
            responseObject[c]['retailer_price']['original'] = product_data['product']['price']['original']
        if not responseObject[c]['retailer_price']:
            responseObject[c].pop('retailer_price')
        responseObject[c]['retailer_affiliate_url'] = product_data['meta']['affiliateUrl']
        responseObject[c]['freight'] = {}
        responseObject[c]['freight']['price'] = product_data['freight']['price']
        responseObject[c]['freight']['delivery_time'] = product_data['freight']['deliveryTime']
        responseObject[c]['freight']['delivery_time'] = product_data['freight']['deliveryTime']
    return responseObject


event = dict()
event['product_aliases'] = {'5712850002794', '7340028726920', '5711008247520', '7340028726926'}
print(lambda_handler(event, event))
